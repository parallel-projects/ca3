#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <sys/time.h>
#include "x86intrin.h"

using std::cout;
using std::endl;

int main()
{
    printf("Student IDs:\n810196518\n810196562\n\n");

    cv::Mat first_img = cv::imread("CA03__Q1__Image__01.png", cv::IMREAD_GRAYSCALE);
    cv::Mat second_img = cv::imread("CA03__Q1__Image__02.png", cv::IMREAD_GRAYSCALE);
    
    unsigned int nrows = first_img.rows;
    unsigned int ncols = first_img.cols;

    cv::Mat serial_diff_img(nrows, ncols, CV_8U);

    unsigned char *first_image;
    unsigned char *second_image;
	unsigned char *serial_diff_image;

	first_image  = (unsigned char *) first_img.data;
	second_image = (unsigned char *) second_img.data;
	serial_diff_image = (unsigned char *) serial_diff_img.data;

    struct timeval serialStart, serialEnd, parallelStart, parallelEnd;

    // Serial implementation   
    gettimeofday(&serialStart, NULL);

    for (int row = 0; row < nrows; row++)
		for (int col = 0; col < ncols; col++)
            *(serial_diff_image + row * ncols + col) = 
                abs(*(second_image + row * ncols + col) - *(first_image + row * ncols + col));
         

    gettimeofday(&serialEnd, NULL);

    long serialSeconds = (serialEnd.tv_sec - serialStart.tv_sec);
    long serialMicros = ((serialSeconds * 1000000) + serialEnd.tv_usec) - (serialStart.tv_usec);
    printf("Serial executaion time %ld s and %ld micros.\n", serialSeconds, serialMicros);

    cv::Mat parallel_diff_img(nrows, ncols, CV_8U);

    __m128i *pFirstSrc;
	__m128i *pSecondSrc;
    __m128i *pDiff;
	__m128i m1, m2, m3, m4, diff;

	pFirstSrc = (__m128i *) first_img.data;
    pSecondSrc = (__m128i *) second_img.data;
	pDiff = (__m128i *) parallel_diff_img.data;

    // Parallel implementation
    gettimeofday(&parallelStart, NULL);

	for (int row = 0; row < nrows; row++)
		for (int col = 0; col < ncols / 16; col++)
		{
			m1 = _mm_loadu_si128(pFirstSrc + row * ncols/16 + col);
            m2 = _mm_loadu_si128(pSecondSrc + row * ncols/16 + col);
            m3 = _mm_subs_epu8(m1, m2);
            m4 = _mm_subs_epu8(m2, m1);
            diff = _mm_or_si128(m3, m4);
			_mm_storeu_si128 (pDiff + row * ncols/16 + col, diff);
		}

    gettimeofday(&parallelEnd, NULL);

    long parallelSeconds = (parallelEnd.tv_sec - parallelStart.tv_sec);
    long parallelMicros = ((parallelSeconds * 1000000) + parallelEnd.tv_usec) - (parallelStart.tv_usec);
    printf("Parallel executaion time %ld s and %ld micros.\n", parallelSeconds, parallelMicros);
    printf("Speed up: %f\n", (float)serialMicros/(float)parallelMicros);

    cv::namedWindow( "serial output", cv::WINDOW_AUTOSIZE ); 	
	cv::imshow( "serial output", serial_diff_img );
	cv::namedWindow( "parallel output", cv::WINDOW_AUTOSIZE );
	cv::imshow( "parallel output", parallel_diff_img );

	cv::waitKey(0);

    return 0;
}