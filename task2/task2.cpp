#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include "x86intrin.h"
#include "time.h"
#include <sys/time.h>


using std::cout;
using std::endl;


void show_output_image(std::string name, cv::Mat image)
{
    cv::namedWindow(name, cv::WINDOW_AUTOSIZE);
    cv::imshow(name, image);
    cv::waitKey(0);
}


int main()
{
    printf("Student IDs:\n810196518\n810196562\n\n");

    // LOAD image
    cv::Mat background_img = cv::imread("CA03__Q2__Image__01.png", cv::IMREAD_GRAYSCALE);
    unsigned int background_ncols = background_img.cols;
    unsigned int background_nrows = background_img.rows;
    cv::Mat transparent_img = cv::imread("CA03__Q2__Image__02.png", cv::IMREAD_GRAYSCALE);
    unsigned int transparent_ncols = transparent_img.cols;
    unsigned int transparent_nrows = transparent_img.rows;

    // Create output images
    cv::Mat serial_image  (background_img.rows, background_img.cols, CV_8U) ;
    cv::Mat parallel_image  (background_img.rows, background_img.cols, CV_8U) ;

    // Pointer to the image data (Matrix of pixels)
    struct timeval serial_start, serial_end, parallel_start, parallel_end;
    gettimeofday(&serial_start, NULL);


    unsigned char *background_img_pointer;
    unsigned char *transparent_img_pointer;
    unsigned char *serial_image_pointer;

    background_img_pointer  = (unsigned char *) background_img.data;
    transparent_img_pointer  = (unsigned char *) transparent_img.data;
    serial_image_pointer = (unsigned char *) serial_image.data;
    
    //serial processing
    for (int row = 0; row < background_nrows; row++){
        for (int col = 0; col < background_ncols; col++){
            if (row > transparent_nrows || col > transparent_ncols)
                *(serial_image_pointer + row * background_ncols + col) = *(background_img_pointer + row * background_ncols + col);
            else{
                *(serial_image_pointer + row * background_ncols + col) = *(background_img_pointer + row * background_ncols + col)
                + (*(transparent_img_pointer + row * transparent_ncols + col) >> 1);
                if((*(background_img_pointer + row * background_ncols + col)
                + (*(transparent_img_pointer + row * transparent_ncols + col) >> 1))>255)
                    *(serial_image_pointer + row * background_ncols + col) = 255;
                if((*(background_img_pointer + row * background_ncols + col)
                + (*(transparent_img_pointer + row * transparent_ncols + col) >> 1))<0)
                    *(serial_image_pointer + row * background_ncols + col) = 0;
            }
        }
    }

    gettimeofday(&serial_end, NULL);
    printf("Serial result:\n");
    long serial_seconds = (serial_end.tv_sec - serial_start.tv_sec);
    long serial_micros = ((serial_seconds * 1000000) + serial_end.tv_usec) - (serial_start.tv_usec);
    printf("serial executaion time is %ld s and %ld micros\n\n", serial_seconds, serial_micros);


//    show_output_image(serial_image);

    
    
    
    
    //parallel processing
    gettimeofday(&parallel_start, NULL);

    __m128i *packed_background_img_pointer;
    __m128i *packed_transparent_img_pointer;
    __m128i *parallel_image_pointer;
    __m128i m1, m2, m3;
    m3 = _mm_set_epi8(127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,127);
    packed_background_img_pointer = (__m128i *) background_img.data;
    packed_transparent_img_pointer = (__m128i *) transparent_img.data;
    parallel_image_pointer = (__m128i *) parallel_image.data;

    for (int row = 0; row < background_nrows; row++){
        for (int col = 0; col < background_ncols/16 ; col+=1){
            m2 = _mm_loadu_si128(packed_background_img_pointer + row * background_ncols/16 + col) ;

            if (row<transparent_nrows && col<transparent_ncols/16){
                m1 = _mm_loadu_si128(packed_transparent_img_pointer + row * transparent_ncols/16 + col);
                m1 = _mm_srli_epi16 (m1, 1);
                m1 = _mm_and_si128 (m1, m3);
                m2 = _mm_adds_epu8 (m1, m2);
            }
            _mm_storeu_si128(parallel_image_pointer + row * background_ncols/16 + col, m2);
        }
    }
    gettimeofday(&parallel_end, NULL);
    printf("Parallel result:\n");
    long parallel_seconds = (parallel_end.tv_sec - parallel_start.tv_sec);
    long parallel_micros = ((parallel_seconds * 1000000) + parallel_end.tv_usec) - (parallel_start.tv_usec);
    printf("serial executaion time is %ld s and %ld micros\n\n", parallel_seconds, parallel_micros);
    printf("speed up: %f\n\n", (float)serial_micros/(float)parallel_micros);


    show_output_image("serial image",serial_image);
    show_output_image("parallel image", parallel_image);


    return 0;
}

